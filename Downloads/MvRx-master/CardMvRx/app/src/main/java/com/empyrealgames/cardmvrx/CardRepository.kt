package com.empyrealgames.cardmvrx

import io.reactivex.Observable


class CardRepository(private val apiService:CardRetrofit){

    fun getCards(): Observable<List<Card>>?{

        return apiService.getCards()
    }

}