package com.empyrealgames.cardmvrx

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
`
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import com.airbnb.mvrx.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_main.*


data class CardState(val cardList: Async<List<Card>> = Uninitialized) : MvRxState


class MvRxCardViewModel(state: CardState, cardRepository: CardRepository) :
    CardViewModel<CardState>(state) {
    init {
        cardRepository.getCards()
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeOn(Schedulers.io())
            ?.execute { copy(cardList = it) }
    }


    companion object : MvRxViewModelFactory<MvRxCardViewModel, CardState> {
        override fun create(
            viewModelContext: ViewModelContext,
            state: CardState
        ): MvRxCardViewModel? {
            val cardRepository = viewModelContext.app<CardApplication>().cardRepository
            return MvRxCardViewModel(state, cardRepository)
        }
    }
}


class CardFragment : BaseMvRxFragment() {

    private val viewModel: MvRxCardViewModel by fragmentViewModel()
    private lateinit var pagerAdapter: PagerAdapter

    override fun invalidate() = withState(viewModel) { state ->
        if (state.cardList.complete) {
            load(state.cardList)

        }

    }

    private fun load(cardList: Async<List<Card>>) {
        pagerAdapter = ScreenSlidePagerAdapter(cardList()!!, activity!!.supportFragmentManager)
        pager.adapter = pagerAdapter

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
         return  inflater.inflate(R.layout.fragment_main, container, false)

    }
}


private class ScreenSlidePagerAdapter(cardList: List<Card>, fragmentManager: FragmentManager) :

    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    var cardList: List<Card>

    init {
        this.cardList = cardList
    }


    override fun getCount(): Int {
        println(cardList.size.toString() + " is size")
        return cardList.size
    }

    override fun getItem(position: Int): Fragment {
        val fragment = ScreenSlidePageFragment()
        val b = Bundle()
        b.putString("DATA", cardList[position].title)
        b.putInt("ID", cardList[position].id)
        fragment.arguments = b
        return fragment
    }

    override fun getItemPosition(`object`: Any): Int {
        return super.getItemPosition(`object`)
    }
}


class ScreenSlidePageFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val rootView = inflater.inflate(
            R.layout.card_fragment, container, false
        )
        val cardTextView = rootView.findViewById<TextView>(R.id.cardTextView)
        cardTextView.text = arguments!!.getString("DATA")
        pageNumberTextView.text = arguments!!.getInt("ID", 0).toString()
        return rootView
    }

}



