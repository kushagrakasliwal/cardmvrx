package com.empyrealgames.cardmvrx

import com.airbnb.mvrx.MvRxState


data class Card(val userId:Int, val id:Int, val title: String, val description:String): MvRxState